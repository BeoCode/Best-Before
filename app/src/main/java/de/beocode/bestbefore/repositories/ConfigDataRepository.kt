package de.beocode.bestbefore.repositories

import androidx.datastore.core.DataStore
import de.beocode.bestbefore.data.ConfigData
import de.beocode.bestbefore.data.Theme
import kotlinx.coroutines.flow.Flow

class ConfigDataRepository(private val configDataStore: DataStore<ConfigData>) {

    fun getData(): Flow<ConfigData> {
        return configDataStore.data
    }

    suspend fun setTheme(theme: Theme) {
        configDataStore.updateData { ConfigData ->
            ConfigData.copy(
                theme = theme
            )
        }
    }

}