package de.beocode.bestbefore.screens

import android.app.DatePickerDialog
import android.content.Context
import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import de.beocode.bestbefore.data.FoodItem
import de.beocode.bestbefore.R
import de.beocode.bestbefore.screens.general.BusyIndicator
import de.beocode.bestbefore.screens.general.TopBarBackSafe
import de.beocode.bestbefore.ui.theme.BestBeforeTheme
import de.beocode.bestbefore.viewmodels.interfaces.LocalUserState
import de.beocode.bestbefore.viewmodels.interfaces.MainViewModelInterface
import de.beocode.bestbefore.viewmodels.previews.MainViewModelPreview
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*

@Composable
fun AddEditScreen(
    navController: NavHostController,
    scope: CoroutineScope,
    index: Int?
) {
    val context = LocalContext.current
    val userState = LocalUserState.current
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }
    val snackHostState = remember { SnackbarHostState() }

    var item: FoodItem? by remember { mutableStateOf(null) }
    var itemIndex: Int? by remember { mutableStateOf(null) }
    var title by remember { mutableStateOf(context.getString(R.string.add_item)) }
    var name by remember { mutableStateOf(TextFieldValue("")) }
    var date by remember { mutableStateOf("") }
    var ts by remember { mutableStateOf(System.currentTimeMillis()) }
    var amount by remember { mutableStateOf(TextFieldValue("")) }
    var place by remember { mutableStateOf(TextFieldValue("")) }

    if (index != null && userState.foodList.size > index) {
        title = context.getString(R.string.update_item)
    }

    LaunchedEffect(key1 = Unit) {
        if (index != null && userState.foodList.size > index) {
            item = userState.foodList[index]
            if (item != null) {
                itemIndex = index
                name = name.copy(item!!.name)
                ts = item!!.ts
                date = convertTsToString(context, ts)
                amount = amount.copy(item!!.amount.toString())
                place = place.copy(item!!.place)
            }
        }
    }

    val addUpdateItem = {
        focusManager.clearFocus()
        addUpdateItem(
            context,
            userState,
            scope,
            snackHostState,
            {
                focusManager.clearFocus()
                navController.popBackStack()
            },
            itemIndex,
            item?.id,
            name.text,
            date,
            ts,
            amount.text.toIntOrNull(),
            place.text
        )
    }

    LaunchedEffect(key1 = Unit) {
        //userState.getSuggestions()
        focusRequester.requestFocus()
    }

    TopBarBackSafe(
        title = title,
        isBusy = userState.isBusy,
        backAction = {
            focusManager.clearFocus()
            navController.popBackStack()
        },
        saveAction = addUpdateItem,
        snackHostState = snackHostState
    ) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp)
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = null
                ) {
                    focusManager.clearFocus()
                },
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            BusyIndicator(isBusy = userState.isBusy)
            AutoCompleteTextField(
                context.getString(R.string.product_name),
                ImeAction.Next,
                Modifier.focusRequester(focusRequester),
                addUpdateItem,
                name,
                userState.nameSuggestions
            ) { name = it }
            OutlinedTextField(
                value = date,
                onValueChange = { },
                modifier = Modifier
                    .fillMaxWidth()
                    .onFocusChanged { change ->
                        if (change.hasFocus)
                            datePicker(context, ts, { ts = it }, { date = it })
                        focusManager.moveFocus(FocusDirection.Down)
                    },
                label = { Text(text = context.getString(R.string.best_before_date)) },
                readOnly = true,
                singleLine = true
            )
            OutlinedTextField(
                value = amount,
                onValueChange = { amount = it },
                modifier = Modifier
                    .fillMaxWidth(),
                label = { Text(text = context.getString(R.string.amount_pcs)) },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Next
                ),
                singleLine = true
            )
            AutoCompleteTextField(
                context.getString(R.string.product_place),
                ImeAction.Done,
                Modifier,
                addUpdateItem,
                place,
                userState.placeSuggestions
            ) { place = it }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun AutoCompleteTextField(
    label: String,
    imeAction: ImeAction,
    modifier: Modifier,
    onDone: () -> Unit,
    value: TextFieldValue,
    suggestions: List<String>,
    valueChange: (TextFieldValue) -> Unit
) {
    var exp by remember { mutableStateOf(true) }
    val filteredSug = suggestions.filter { it.contains(value.text, true) }

    ExposedDropdownMenuBox(
        expanded = exp,
        onExpandedChange = { exp = !exp },
        modifier = Modifier.fillMaxWidth()
    ) {
        OutlinedTextField(
            value = value,
            onValueChange = valueChange,
            modifier = modifier
                .fillMaxWidth()
                .onFocusChanged {
                    exp = it.hasFocus
                },
            label = { Text(text = label) },
            keyboardOptions = KeyboardOptions(imeAction = imeAction),
            keyboardActions = KeyboardActions(onDone = { onDone() }),
            singleLine = true
        )
        if (filteredSug.isNotEmpty()) {
            ExposedDropdownMenu(
                expanded = exp,
                onDismissRequest = { exp = false }
            ) {
                filteredSug.forEach {
                    DropdownMenuItem(
                        onClick = {
                            valueChange(value.copy(it, TextRange(it.length)))
                            exp = false
                        }
                    ) {
                        Text(text = it, overflow = TextOverflow.Ellipsis, maxLines = 1)
                    }
                }
            }
        }
    }
}

fun datePicker(context: Context, ts: Long, setTs: (Long) -> Unit, setDate: (String) -> Unit) {
    val cal = Calendar.getInstance(Locale.getDefault())
    cal.timeInMillis = ts

    val datePicker = DatePickerDialog(context, { _, y, m, d ->
        val month = if (m + 1 > 9) "${m + 1}" else "0${m + 1}"
        setDate(String.format(context.getString(R.string.date), d, month, y))
        val calendar = Calendar.getInstance(Locale.getDefault())
        calendar.set(y, m, d)
        setTs(calendar.timeInMillis)
    }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
    datePicker.show()
}

fun convertTsToString(context: Context, ts: Long): String {
    val cal = Calendar.getInstance(Locale.getDefault())
    cal.timeInMillis = ts
    return String.format(
        context.getString(R.string.date),
        cal.get(Calendar.DAY_OF_MONTH),
        cal.get(Calendar.MONTH) + 1,
        cal.get(Calendar.YEAR)
    )
}

fun addUpdateItem(
    context: Context,
    userState: MainViewModelInterface,
    scope: CoroutineScope,
    snackHostState: SnackbarHostState,
    success: () -> Unit,
    index: Int?,
    id: Long?,
    name: String,
    date: String,
    ts: Long,
    amount: Int?,
    place: String
) {
    scope.launch {
        if (name.isNotBlank() && date.isNotBlank() && amount != null && amount > 0) {
            var item = FoodItem(
                name = name,
                ts = ts,
                amount = amount,
                place = place
            )

            if (index == null || id == null) {
                userState.addFood(item) { success ->
                    if (success) {
                        success()
                    } else {
                        scope.launch {
                            snackHostState.showSnackbar(
                                message = context.getString(R.string.error_occurred),
                                duration = SnackbarDuration.Short
                            )
                        }
                    }
                }
            } else {
                item = item.copy(id = id)
                userState.updateFood(index, item) { success ->
                    if (success) {
                        success()
                    } else {
                        scope.launch {
                            snackHostState.showSnackbar(
                                message = context.getString(R.string.error_occurred),
                                duration = SnackbarDuration.Short
                            )
                        }
                    }
                }
            }
        } else {
            scope.launch {
                snackHostState.showSnackbar(
                    message = context.getString(R.string.fill_fields_out),
                    duration = SnackbarDuration.Short
                )
            }
        }
    }
}

@Preview(name = "LightMode", showBackground = true)
@Preview(name = "DarkMode", showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DefaultPreview() {
    val navController = rememberNavController()
    val scope = rememberCoroutineScope()
    BestBeforeTheme {
        CompositionLocalProvider(LocalUserState provides MainViewModelPreview()) {
            AddEditScreen(navController = navController, scope = scope, index = null)
        }
    }
}
