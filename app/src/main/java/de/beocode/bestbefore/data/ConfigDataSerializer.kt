package de.beocode.bestbefore.data

import android.content.Context
import androidx.datastore.core.Serializer
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.dataStore
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.io.OutputStream

object ConfigDataSerializer : Serializer<ConfigData> {
    override val defaultValue: ConfigData
        get() = ConfigData()

    override suspend fun readFrom(input: InputStream): ConfigData {
        return try {
            Json.decodeFromString(
                deserializer = ConfigData.serializer(),
                string = input.readBytes().decodeToString()
            )
        } catch (e: SerializationException) {
            e.printStackTrace()
            defaultValue
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun writeTo(t: ConfigData, output: OutputStream) {
        output.write(
            Json.encodeToString(
                serializer = ConfigData.serializer(),
                value = t
            ).encodeToByteArray()
        )
    }

}

val Context.configDataStore by dataStore(
    fileName = "config-data.json",
    serializer = ConfigDataSerializer,
    corruptionHandler = ReplaceFileCorruptionHandler(
        produceNewData = { ConfigData() }
    )
)