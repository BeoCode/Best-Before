package de.beocode.bestbefore.data

import de.beocode.bestbefore.R
import kotlinx.serialization.Serializable

@Serializable
data class ConfigData(
    val theme: Theme = Theme.System
)

enum class Theme(val keyName: Int){
    System(R.string.system),
    Light(R.string.light),
    Dark(R.string.dark)
}