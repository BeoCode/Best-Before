package de.beocode.bestbefore.ui.theme

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors

val DarkColorPalette = darkColors(
    primary = Orange,
    primaryVariant = LightGray,
    secondary = Orange,
    background = DarkGray3,
    surface = DarkGray2,
    error = Red,
    onPrimary = DarkGray3,
    onSecondary = DarkGray3,
    onBackground = White,
    onSurface = LightGray,
    onError = DarkGray1
)

val LightColorPalette = lightColors(
    primary = Orange,
    primaryVariant = DarkGray1,
    secondary = Orange,
    background = White,
    surface = LightGray,
    error = Red,
    onPrimary = DarkGray3,
    onSecondary = DarkGray3,
    onBackground = DarkGray3,
    onSurface = DarkGray2,
    onError = LightGray
)