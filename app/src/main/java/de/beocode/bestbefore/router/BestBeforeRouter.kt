package de.beocode.bestbefore.router

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import de.beocode.bestbefore.screens.AddEditScreen
import de.beocode.bestbefore.screens.MainScreen
import de.beocode.bestbefore.screens.SettingsScreen
import kotlinx.coroutines.CoroutineScope

@Composable
fun BestBeforeRouter(
    scope: CoroutineScope
) {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.Main.route
    ) {
        composable(
            route = Screens.Main.route
        ) {
            MainScreen(navController)
        }
        composable(
            route = Screens.AddEdit.route + Screens.AddEdit.arguments,
            arguments = listOf(navArgument("index") {
                nullable = true
            })
        ) { backStackEntry ->
            AddEditScreen(
                navController,
                scope,
                backStackEntry.arguments?.getString("index")?.toIntOrNull()
            )
        }
        composable(
            route = Screens.Settings.route
        ) {
            SettingsScreen(navController, scope)
        }
    }
}