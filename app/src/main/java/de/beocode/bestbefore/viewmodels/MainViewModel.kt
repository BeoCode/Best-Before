package de.beocode.bestbefore.viewmodels

import android.app.Application
import android.net.Uri
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.beocode.bestbefore.data.*
import de.beocode.bestbefore.repositories.AppDataRepository
import de.beocode.bestbefore.repositories.ConfigDataRepository
import de.beocode.bestbefore.viewmodels.interfaces.MainViewModelInterface
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject

private const val LOG_TAG = "BBMainViewModel"

@HiltViewModel
class MainViewModel @Inject constructor(private val application: Application) :
    MainViewModelInterface, ViewModel() {
    private val configRepository = ConfigDataRepository(application.configDataStore)
    private val dataRepository = AppDataRepository(application.appDataStore)

    override var isBusy by mutableStateOf(false)

    override var appTheme by mutableStateOf(Theme.System)
    override var expireWarning by mutableStateOf(0)
    override var foodList by mutableStateOf(persistentListOf<FoodItem>())
    override var filteredFoodList by mutableStateOf(emptyList<FoodItem>())
    override var nameSuggestions by mutableStateOf(persistentListOf<String>())
    override var placeSuggestions by mutableStateOf(persistentListOf<String>())

    init {
        isBusy = true
        viewModelScope.launch {
            configRepository.getData()
                .collect { configData ->
                    appTheme = configData.theme
                }
        }
        viewModelScope.launch {
            dataRepository.getData()
                .catch { Log.e(LOG_TAG, it.stackTraceToString()) }
                .collect { appData ->
                    expireWarning = appData.warningDays
                    gotFoodList(appData.foodList)
                    nameSuggestions = appData.nameSuggestions.toPersistentList()
                    Log.d(LOG_TAG, "Got ${nameSuggestions.size} name suggestions")
                    placeSuggestions = appData.placeSuggestions.toPersistentList()
                    Log.d(LOG_TAG, "Got ${placeSuggestions.size} name suggestions")
                }
        }
    }

    private fun gotFoodList(list: List<FoodItem>) {
        foodList = list.toPersistentList()
        filteredFoodList = foodList
        Log.d(LOG_TAG, "Got ${foodList.size} items")
        isBusy = false
    }

    override fun searchFoodList(search: String) {
        filteredFoodList = foodList.filter {
            it.name.contains(search, true)
        }
    }

    override fun addFood(foodItem: FoodItem, success: (Boolean) -> Unit) {
        isBusy = true
        viewModelScope.launch {
            try {
                dataRepository.addFood(foodItem)
                Log.d(LOG_TAG, "Added ${foodItem.name}")
                success(true)
            } catch (e: Exception) {
                e.printStackTrace()
                isBusy = false
                success(false)
            }
        }
        addSuggestions(foodItem.name, foodItem.place)
    }

    override fun updateFood(index: Int, foodItem: FoodItem, success: (Boolean) -> Unit) {
        isBusy = true
        viewModelScope.launch {
            try {
                dataRepository.updateFood(index, foodItem)
                Log.d(LOG_TAG, "Updated ${foodItem.name}")
                success(true)
            } catch (e: Exception) {
                e.printStackTrace()
                isBusy = false
                success(false)
            }
        }
        addSuggestions(foodItem.name, foodItem.place)
    }

    override fun deleteFood(foodItem: FoodItem) {
        isBusy = true
        viewModelScope.launch {
            val index = foodList.indexOf(foodItem)
            dataRepository.deleteFood(index)
            Log.d(LOG_TAG, "Deleted ${foodItem.name}")
        }
    }

    override fun addSuggestions(name: String, destination: String) {
        viewModelScope.launch {
            if (!nameSuggestions.contains(name))
                dataRepository.addName(name)
            if (!placeSuggestions.contains(destination) && destination.isNotBlank())
                dataRepository.addDestination(destination)
        }
    }

    override fun setWarningDays(days: Int) {
        Log.d(LOG_TAG, "Set expire warning to $days days")
        viewModelScope.launch {
            dataRepository.setExpireWarning(days)
        }
    }

    override fun setTheme(theme: Theme) {
        Log.d(LOG_TAG, "Set theme: $theme")
        viewModelScope.launch {
            configRepository.setTheme(theme)
        }
    }

    override suspend fun importData(uri: Uri?, success: (Boolean) -> Unit) {
        if (uri != null) {
            val contentResolver = application.contentResolver

            try {
                contentResolver.openInputStream(uri).use { inputStream ->
                    if (inputStream != null) {
                        var newData = Json.decodeFromString(
                            deserializer = AppData.serializer(),
                            string = inputStream.readBytes().decodeToString()
                        )
                        // To prevent duplicated ids
                        val newFoodList: MutableList<FoodItem> = mutableListOf()
                        for ((i, food) in newData.foodList.withIndex()) {
                            newFoodList.add(food.copy(id = i.toLong()))
                        }
                        newData = newData.copy(foodList = newFoodList)
                        dataRepository.importData(newData)
                        success(true)
                        return
                    }
                }
            } catch (e: FileNotFoundException) {
                Log.e(LOG_TAG, e.stackTraceToString())
            } catch (e: SerializationException) {
                Log.e(LOG_TAG, e.stackTraceToString())
            } catch (e: Exception) {
                Log.e(LOG_TAG, e.stackTraceToString())
            }
        }
        success(false)
    }

    override suspend fun exportData(uri: Uri?, success: (Boolean) -> Unit) {
        if (uri != null) {
            val contentResolver = application.contentResolver

            try {
                contentResolver.openFileDescriptor(uri, "w").use { parcelDescriptor ->
                    if (parcelDescriptor != null) {
                        FileOutputStream(parcelDescriptor.fileDescriptor).use { outputStream ->
                            outputStream.write(
                                Json.encodeToString(
                                    serializer = AppData.serializer(),
                                    value = dataRepository.getData().first()
                                ).encodeToByteArray()
                            )
                            success(true)
                            return
                        }
                    }
                }
            } catch (e: FileNotFoundException) {
                Log.e(LOG_TAG, e.stackTraceToString())
            } catch (e: IOException) {
                Log.e(LOG_TAG, e.stackTraceToString())
            } catch (e: Exception) {
                Log.e(LOG_TAG, e.stackTraceToString())
            }
        }
        success(false)
    }

    override suspend fun clearData(success: (Boolean) -> Unit) {
        isBusy = true
        try {
            dataRepository.clearData()
            Log.d(LOG_TAG, "Deleted all data")
            success(true)
        } catch (e: Exception) {
            e.printStackTrace()
            success(false)
        }
        isBusy = false
    }
}