package de.beocode.bestbefore.viewmodels.previews

import android.net.Uri
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import de.beocode.bestbefore.data.FoodItem
import de.beocode.bestbefore.data.Theme
import de.beocode.bestbefore.viewmodels.interfaces.MainViewModelInterface
import kotlinx.collections.immutable.persistentListOf

class MainViewModelPreview : MainViewModelInterface, ViewModel() {

    override var isBusy by mutableStateOf(false)

    override var appTheme: Theme by mutableStateOf(Theme.System)
    override var expireWarning by mutableStateOf(0)
    override var foodList by mutableStateOf(persistentListOf<FoodItem>())
    override var filteredFoodList by mutableStateOf(emptyList<FoodItem>())
    override var nameSuggestions by mutableStateOf(persistentListOf<String>())
    override var placeSuggestions by mutableStateOf(persistentListOf<String>())

    override fun searchFoodList(search: String) { }

    override fun addFood(foodItem: FoodItem, success: (Boolean) -> Unit) { }
    override fun updateFood(index: Int, foodItem: FoodItem, success: (Boolean) -> Unit) { }
    override fun deleteFood(foodItem: FoodItem) { }

    override fun addSuggestions(name: String, destination: String) { }

    override fun setWarningDays(days: Int) { }

    override fun setTheme(theme: Theme) { }

    override suspend fun importData(uri: Uri?, success: (Boolean) -> Unit) { }
    override suspend fun exportData(uri: Uri?, success: (Boolean) -> Unit) { }

    override suspend fun clearData(success: (Boolean) -> Unit) { }
}