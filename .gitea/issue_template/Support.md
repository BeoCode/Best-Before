---

name: "Support"
about: "Use this form if you need help"
title: "[SUP] "
labels:

- support

---

### What is your problem?
<!--Please describe precisely.-->

<!-- Depending on the question helpful -->
### Environment data
- Android version:

- Device model:

- Stock or customized system:

- Best-Before app version:

### Relevant  screenshots
<!--Paste any relevant screenshots-->
